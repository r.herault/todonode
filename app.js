const express = require('express');
const app = express();
const db = require('./db');
const todo = require('./routes/todo');

const path = __dirname + '/views/';
const port = process.env.PORT || 8080;

app.engine('pug', require('pug').renderFile);
app.set('view engine', 'pug');
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path));
app.use('/', todo);


app.listen(port, function() {
  console.log(`Serveur à l'écoute sur le port ${port}`);
})
