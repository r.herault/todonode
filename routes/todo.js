const express = require('express');
const router = express.Router();
const todo = require('../controllers/todo');

router.get('/todo', function(req, res){
  todo.list(req, res);
});

router.post('/todo/add', function(req, res) {
  todo.create(req,res);
});

router.get('/todo/edit/:id', function(req, res) {
  todo.edit(req, res);
});

router.post('/todo/edit/:id', function(req, res) {
  todo.edit(req, res);
});

router.get('/todo/delete/:id', function(req, res) {
  todo.delete(req, res);
});

router.get('/todo/finish/:id/:finish', function(req, res) {
  todo.finish(req, res);
});

router.get('/todo/sort/:col', function(req, res) {
  todo.sort(req, res);
});

router.get('/todo/export/:type', function(req, res) {
  todo.export(req, res);
});

router.get('*', function(req, res) {
  todo.not_found(req, res);
});

module.exports = router;