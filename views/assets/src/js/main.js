// set date to datepicker
let dateObj = new Date();
// let day = dateObj.getDate();
let day = String(dateObj.getDate()).padStart(2, '0');
let month = ('0' + (dateObj.getMonth() + 1)).slice(-2);
let year = dateObj.getFullYear();

let date_now = year + "-" + month + "-" + day;
dateObj.setDate(dateObj.getDate() + 1);
let date_tomorrow = year + "-" + month + "-" + String(dateObj.getDate()).padStart(2, '0');

document.querySelector('input[name="date"]').setAttribute('min', date_now)

let element = document.querySelector('body');
if(! element.classList.contains('edit')) {
  document.querySelector('input[name="date"]').setAttribute('value', date_tomorrow)
}

function confirmSuppr(id){
  let confirmRes = confirm('Souhaitez-vous réellement supprimer cette tâche ?');
  if (confirmRes == true) {
    location.href = "/todo/delete/"+id;
  }
}

function todoCheck(id, checkbox) {
  location.href = "/todo/finish/" + id + "/" + checkbox.checked;
}

document.querySelectorAll('th:not(:last-child)').forEach(th => th.addEventListener('click', () =>{
  location.href = "/todo/sort/" + th.textContent;
}));