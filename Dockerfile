FROM node:latest

# ENV https_proxy=http://wwwcache.univ-orleans.fr:3128
# ENV http_proxy=http://wwwcache.univ-orleans.fr:3128
# ENV no_proxy=servinfo-docker

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . . 

RUN npm install

RUN npm run build

EXPOSE 8080

CMD  ["node", "app.js"]