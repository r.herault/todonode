const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Todo = new Schema ({
  name: { type: String, required: true },
  date: {
    type: Date,
    required: false,
  },
  category: { type: String, required: false },
  finished: { type: Boolean, required: true, default: false}
});

module.exports = mongoose.model('Todo', Todo)
