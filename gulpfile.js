const gulp = require("gulp"),
  sass         = require("gulp-sass"),
  sourcemaps   = require("gulp-sourcemaps"),
  concat       = require("gulp-concat"),
  pump         = require("pump");

function css() {
  const postcss = require('gulp-postcss');
  return gulp
    .src('views/assets/src/scss/style.scss')
    .pipe(sourcemaps.init())
		.pipe(concat("style.css"))
		.pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(postcss([
      require('tailwindcss'),
      require('autoprefixer'),
    ]))
		.pipe(sourcemaps.write())
    .pipe(gulp.dest('views/assets/dist/css'));
}

function js() {
	return pump([
		gulp.src('views/assets/src/js/main.js'),
		sourcemaps.init(),
		concat("script.js"),
		sourcemaps.write(),
		gulp.dest('views/assets/dist/js')
	]);
}

function watch() {
  gulp.watch('views/assets/src/**/*.scss', css);
  gulp.watch('views/assets/src/**/*.js', js);
}

exports.default = gulp.parallel(css, js);
exports.css = css;
exports.js = js;
exports.watch = watch;
