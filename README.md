# Todo Node :tada:
> Todo Node is a task management application with Express

## How to run ?
### With docker :whale:
To launch application : 
`docker-compose up -d`

### Manually
Install dependencies :
`yarn`

Compile front files :
`yarn build`

Launch application :
`node app.js`

## How to use ?
Go to [localhost:8080/todo](http://localhost:8080/todo)

## Developers
* **Romain HERAULT** - *Developer* - [r.herault](https://rherault.fr)
* **Florian Dubois** - *Developer*
* **Guillaume Turpin** - *Developer*